#ifndef SCANNEREMULATOR_H
#define SCANNEREMULATOR_H

#include <QObject>
#include <QTcpServer>
#include <QSignalMapper>

class ScannerEmulator : public QObject
{
    Q_OBJECT
public:
    explicit ScannerEmulator(QObject *parent = nullptr);
    ~ScannerEmulator();

signals:
    void addLog(QString);
    void printerStart();
    void printerPause();
public slots:
    void send(QString cmd);
    void listen(int port);
private slots:
    void newClient();
    void dataFromClient(QObject *sender);
    void clientDisconnected(QObject * sender);
private:
    uint idClient;
    QTcpServer *server;
    QVector<QTcpSocket*> clients;
    QSignalMapper *mapperClientsRead, *mapperClientsDisconnect;
};

#endif // SCANNEREMULATOR_H
