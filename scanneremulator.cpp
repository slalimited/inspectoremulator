#include "scanneremulator.h"
#include <QTcpSocket>

ScannerEmulator::ScannerEmulator(QObject *parent) : QObject(parent)
{
    server = new QTcpServer(this);
    mapperClientsRead = new QSignalMapper(this);
    mapperClientsDisconnect = new QSignalMapper(this);
    connect(server, SIGNAL(newConnection()), this, SLOT(newClient()));
    idClient = 1;
}

ScannerEmulator::~ScannerEmulator()
{
    server->close();
    foreach (QTcpSocket* client, clients) {
        client->disconnectFromHost();
        client->deleteLater();
    }
    delete server;
    delete mapperClientsRead;
    delete mapperClientsDisconnect;
}
void ScannerEmulator::send(QString cmd)
{
    QVector<QTcpSocket*> notActive;
    for(int i = 0; i < clients.size(); i++)
    {
        QTcpSocket *client = clients.at(i);
        if(client->state() == QAbstractSocket::ConnectedState)
        {
                client->write(cmd.toLatin1());
        }
        else
            notActive.push_back(client);
    }
    //Remove not active clients from list
    for(int i = 0; i < notActive.size(); i++)
        clients.removeOne(notActive[i]);
}

void ScannerEmulator::listen(int port)
{
    server->listen(QHostAddress::Any, port);
}

void ScannerEmulator::newClient()
{
    while(server->hasPendingConnections())
    {
        QTcpSocket *client = server->nextPendingConnection();
        clients.push_back(client);
        client->setObjectName(QString::number(idClient++));
        connect(client, SIGNAL(readyRead()),
                mapperClientsRead, SLOT(map()));
        mapperClientsRead->setMapping(client, client);
        connect(client, SIGNAL(disconnected()),
                mapperClientsDisconnect, SLOT(map()));
        mapperClientsDisconnect->setMapping(client, client);
        emit addLog("Подключен новый клиент: " + client->objectName());
    }
    connect(mapperClientsRead, SIGNAL(mapped(QObject*)),
            this, SLOT(dataFromClient(QObject*)));
    connect(mapperClientsDisconnect, SIGNAL(mapped(QObject*)),
            this, SLOT(clientDisconnected(QObject*)));
}

void ScannerEmulator::clientDisconnected(QObject *sender)
{
    QTcpSocket *client = qobject_cast<QTcpSocket *>(sender);
    if(clients.contains(client))
    {
        emit addLog("Клиент отключился : " + client->objectName());
        clients.removeOne(client);
    }
}

void ScannerEmulator::dataFromClient(QObject *sender)
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender);
    QByteArray data;
    while (socket->bytesAvailable())
    {
        QByteArray buf = socket->readAll();
        data.append(buf);
    }
    if(data.size() > 0)
    {
        emit addLog(tr("Получено от клиента: %1").arg(QString::fromLatin1(data)));
        QString d = QString::fromUtf8(data);
        if(d.contains("!!system.pause"))
            emit printerPause();
        if(d.contains("!!system.resume"))
            emit printerStart();
        if(d.contains("!!system.info.id?"))
            socket->write(QString("\x1b\"23\"\x0d\x0a").toUtf8());
    }
}
