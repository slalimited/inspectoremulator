#ifndef EMULATOR_H
#define EMULATOR_H

#include <QObject>
#include <QDateTime>
#include <QTimer>
#include <QFile>
#include <QTextStream>
#include <QDebug>

class emulator : public QObject
{
    Q_OBJECT
public:
    emulator(int codesCount, int codeLength, int startCodeIndex,
             int startFileCodeIndex, int fileCodeLength, int millisec,
             int filesCount, QString catalog);
    ~emulator();
public slots:
    void startEmulating(int count);
    void stopEmulating();
    void periodChanged(int);
signals:
    void startCodeIndexChanged(int);
    void startFileCodeIndexChanged(int);
    void logMessage(QString);
    void finished();
    void fileSaved(QString code);
private slots:
    QString addZeros(QString str, int strLength);
    QString fileName();
    QStringList codesList();
    void saveFile();
    void continueEmulating();
        void createTimer();
private:
    int codesCount;
    int codeLength;
    int currentCodeIndex;
    int currentFileCodeIndex;
    int fileCodeLength;
    int millisec;
    int currentfilesCount;
    int stopFilesCount;
    QTimer *timer;
    QString catalog;

};

#endif // EMULATOR_H
