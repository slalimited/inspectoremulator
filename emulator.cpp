#include "emulator.h"

emulator::emulator(int codesCount, int codeLength, int startCodeIndex,
                   int startFileCodeIndex, int fileCodeLength, int timeout,
                   int filesCount,
                   QString catalog)
{
    this->codesCount = codesCount;
    this->codeLength = codeLength;
    this->currentCodeIndex = startCodeIndex;
    this->currentFileCodeIndex = startFileCodeIndex;
    this->millisec = timeout;
    this->fileCodeLength = fileCodeLength;
    stopFilesCount = filesCount;
    currentfilesCount = 0;
    this->catalog = catalog;
    createTimer();
    qDebug() << "Emulator created. Files" << filesCount << catalog;
}

void emulator::createTimer()
{
    timer = new QTimer(this);
    timer->setInterval(millisec);
    timer->setTimerType(Qt::PreciseTimer);
    connect(timer, SIGNAL(timeout()), this, SLOT(continueEmulating()));
}

emulator::~emulator()
{
    delete timer;
}

QString emulator::fileName()
{
    return QDate::currentDate().toString("yyyyMMdd")+"_"+
            addZeros(QString::number(currentFileCodeIndex),
                     fileCodeLength)+".txt";
}

QString emulator::addZeros(QString str, int strLength)
{
    while(str.length() < strLength)
        str = "0"+str;
    return str;
}

QStringList emulator::codesList()
{
    QStringList codes;
    for (int i = currentCodeIndex; i < currentCodeIndex+codesCount; ++i)
        codes << addZeros(QString::number(i), codeLength);
    return codes;
}

void emulator::saveFile()
{
    QString fName = catalog+"/"+fileName();
    QFile file(fName);
    QTextStream out(&file);
    if (!file.open(QIODevice::WriteOnly)) {
        emit logMessage(tr("Ошибка открытия файла %1").arg(fName));
        timer->stop();
        emit finished();
    }
    out << codesList().join("\n") << "\n";
    emit logMessage(fileName());
    emit fileSaved(addZeros(QString::number(currentFileCodeIndex),
                            fileCodeLength));
    currentCodeIndex+=codesCount;
    emit startCodeIndexChanged(currentCodeIndex);
    currentFileCodeIndex++;
    emit startFileCodeIndexChanged(currentFileCodeIndex);
    file.close();
    if (++currentfilesCount == stopFilesCount) {
        timer->stop();
        emit logMessage(tr("Генерация окончена: сформировано %1 файлов")
                        .arg(currentfilesCount));
        emit finished();
    }
}

void emulator::continueEmulating()
{
    saveFile();
}

void emulator::startEmulating(int count)
{
    stopFilesCount = count;
    emit logMessage(tr("Начата генерация %1 файлов в каталог %2").arg(count)
                    .arg(catalog));
    timer->start();
}

void emulator::stopEmulating()
{
    timer->stop();
}

void emulator::periodChanged(int interval)
{
    timer->setInterval(interval);
}
