#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QThread>
#include <QFileDialog>
#include <QSettings>
#include "emulator.h"
#include "scanneremulator.h"
#include <QQueue>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
signals:
    void    started(int count);
    void    paused();
    void finished();
    void    periodChanged(int);
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void logMessage(QString msg);
    void startCodeIndexChanged(int currentValue);
    void startFileCodeIndexChanged(int currentValue);
    void createObjects();
    void on_ok_clicked();

    void on_toolButton_clicked();
    void sendCode();
    void readValues();
    void saveValues();
    void fileSaved(QString);
    void start();
    void pause();
    void on_pushButton_clicked();

    void on_timeout_valueChanged(int arg1);

private:
    QTimer      *timer;
        Ui::MainWindow *ui;
    int currentCodeNumber;
    ScannerEmulator box, pallete;
    ScannerEmulator printerBox;
    int counterBox;
    QQueue<QString>      queue;
};



#endif // MAINWINDOW_H
