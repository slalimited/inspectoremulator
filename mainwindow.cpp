#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer = new QTimer(this);
    timer->setInterval(ui->timeout->value());
    connect(timer, &QTimer::timeout, this, &MainWindow::sendCode);
    readValues();
    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(saveValues()));
    box.listen(ui->scannerBoxPort->value());
    pallete.listen(ui->scannerPalletePort->value());
    printerBox.listen(10900);
    //connect(&printerBox, &ScannerEmulator::addLog,
    //        this, &MainWindow::logMessage);
    connect(&printerBox, &ScannerEmulator::printerPause,
            this, &MainWindow::pause);
    connect(&printerBox, &ScannerEmulator::printerStart,
            this, &MainWindow::start);
    counterBox = 0;
    //createObjects();
}

MainWindow::~MainWindow()
{
    emit finished();
    delete timer;
    delete ui;

}

void MainWindow::readValues()
{
    QSettings settings(QCoreApplication::applicationDirPath()+"\\setting.ini",
                       QSettings::IniFormat);
    settings.beginGroup("initValues");
    ui->codesCount->setValue(settings.value("codesCount", 2).toInt());
    ui->codeLength->setValue(settings.value("codeLength", 15).toInt());
    ui->startCodeIndex->setValue(settings.value("startCodeIndex", 1).toInt());
    ui->startFileCodeIndex->setValue(settings.value("startFileCodeIndex").toInt());
    ui->fileCodeLength->setValue(settings.value("fileCodeLength", 26).toInt());
    ui->timeout->setValue(settings.value("timeout", 1000).toInt());
    ui->filesCount->setValue(settings.value("filesCount", 10).toInt());
    ui->dir->setText(settings.value("dir").toString());
    ui->scannerBoxPort->setValue(settings.value("scannerBoxPort", 10200).toInt());
    ui->scannerPalletePort->setValue(settings.value("scannerPalPort", 10400).toInt());
    ui->scannerBoxDelay->setValue(settings.value("scannerBoxDelay", 1).toInt());
    ui->scannerBoxEnable->setChecked(settings.value("scannerBoxOn").toBool());
    ui->scannerPalleteEnable->setChecked(settings.value("scannerPalOn").toBool());
    ui->palleteStartCode->setText(settings.value("scannerPalStart").toString());
    ui->countInPallete->setValue(settings.value("scannerPalCount").toInt());
    ui->leFilenameTemplate->setText(QString("yyyyMMdd_%1.txt")
                                    .arg(QString().fill('E', ui->fileCodeLength->value())));
    settings.endGroup();
}

void MainWindow::saveValues()
{
    QSettings settings(QCoreApplication::applicationDirPath()+"\\setting.ini",
                       QSettings::IniFormat);
    settings.beginGroup("initValues");
    settings.setValue("codesCount", ui->codesCount->value());
    settings.setValue("codeLength", ui->codeLength->value());
    settings.setValue("startCodeIndex", ui->startCodeIndex->value());
    settings.setValue("startFileCodeIndex", ui->startFileCodeIndex->value());
    settings.setValue("fileCodeLength", ui->fileCodeLength->value());
    settings.setValue("timeout", ui->timeout->value());
    settings.setValue("filesCount", ui->filesCount->value());
    settings.setValue("dir", ui->dir->text());
    settings.setValue("scannerBoxPort",ui->scannerBoxPort->value());
    settings.setValue("scannerPalPort", ui->scannerPalletePort->value());
    settings.setValue("scannerBoxDelay",ui->scannerBoxDelay->value());
    settings.setValue("scannerBoxOn",ui->scannerBoxEnable->isChecked());
    settings.setValue("scannerPalOn",ui->scannerPalleteEnable->isChecked());
    settings.setValue("scannerPalStart",ui->palleteStartCode->text());
    settings.setValue("scannerPalCount",ui->countInPallete->value());
    settings.endGroup();
}

void MainWindow::fileSaved(QString code)
{
    if(ui->scannerBoxEnable->isChecked())

    {
        QTimer::singleShot(ui->scannerBoxDelay->value()*1000, [this, code](){
                    queue.push_back(code);
        });
    }
}

void MainWindow::start()
{
    createObjects();
    logMessage(tr("Генерация запущена принтером"));
    emit started(ui->filesCount->value());
    timer->start();
}

void MainWindow::pause()
{
    emit finished();
    logMessage(tr("Генерация приостановлена принтером"));
    emit paused();
    timer->stop();
}

void MainWindow::createObjects()
{
    emulator *emul = new emulator(ui->codesCount->value(),
                                  ui->codeLength->value(),
                                  ui->startCodeIndex->value(),
                                  ui->startFileCodeIndex->value(),
                                  ui->fileCodeLength->value(),
                                  ui->timeout->value(),
                                  ui->filesCount->value(),
                                  ui->dir->text());
    QThread *thrd = new QThread;
    emul->moveToThread(thrd);
    connect(this, &MainWindow::finished, thrd, &QThread::quit);
    connect(thrd, &QThread::finished, emul, &QObject::deleteLater);
    connect(this, &MainWindow::started, emul, &emulator::startEmulating);
    connect(this, &MainWindow::paused, emul, &emulator::stopEmulating);
    connect(this, &MainWindow::periodChanged, emul, &emulator::periodChanged);
    //connect(thrd, &QThread::started, emul, &emulator::createTimer);
    connect(emul, SIGNAL(logMessage(QString)), this, SLOT(logMessage(QString)));
    connect(emul, SIGNAL(startCodeIndexChanged(int)),
            this, SLOT(startCodeIndexChanged(int)));
    connect(emul, SIGNAL(startFileCodeIndexChanged(int)),
            this, SLOT(startFileCodeIndexChanged(int)));
    connect(emul, &emulator::fileSaved,
            this, &MainWindow::fileSaved);
    thrd->start();

}

void MainWindow::logMessage(QString msg)
{
    ui->log->insertPlainText(QDateTime::currentDateTime()
                             .toString("dd.MM.yyyy hh:mm:ss:zz")+"\t"+msg+"\n");
    QTextCursor cursor = ui->log->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->log->setTextCursor(cursor);
}

void MainWindow::startCodeIndexChanged(int currentValue)
{
    ui->startCodeIndex->setValue(currentValue);
}

void MainWindow::startFileCodeIndexChanged(int currentValue)
{
    ui->startFileCodeIndex->setValue(currentValue);
}

void MainWindow::on_ok_clicked()
{
    createObjects();
    logMessage(tr("Генерация запущена пользователем"));
    emit started(ui->filesCount->value());
    timer->start();
}

void MainWindow::on_toolButton_clicked()
{
    ui->dir->setText(QFileDialog::getExistingDirectory(
                         this, tr("Выбор каталога"),
                         QCoreApplication::applicationDirPath(),
                         QFileDialog::ShowDirsOnly
                         | QFileDialog::DontResolveSymlinks));
}

void MainWindow::sendCode()
{
    if(queue.isEmpty())
        return;
    QString code = queue.takeFirst();
    box.send("<"+code+">");
    logMessage("Сканер коробов отправлен код короба " + code);
    counterBox++;
    if(ui->scannerPalleteEnable->isChecked() &&
            counterBox == ui->countInPallete->value())
    {
        counterBox = 0;
        QTimer::singleShot(1000, [this, code](){
            this->pallete.send("<"+code+">");
            this->logMessage("Сканер паллет отправлен код короба " + code);
             QTimer::singleShot(1000, [this](){
            this->pallete.send("<"+this->ui->palleteStartCode->text()+">");
            logMessage("Сканер паллет отправлен код паллеты " + this->ui->palleteStartCode->text());
            bool ok;
            int palCode = this->ui->palleteStartCode->text().toInt(&ok);
            if(ok)
                this->ui->palleteStartCode->setText(QString::number(palCode+1));
            else
                this->ui->palleteStartCode->setText("1");
             });
        });
    }
}

void MainWindow::on_pushButton_clicked()
{
    logMessage(tr("Генерация приостановлена пользователем"));
    emit paused();
    emit finished();
}

void MainWindow::on_timeout_valueChanged(int interval)
{
    timer->setInterval(interval);
    emit periodChanged(interval);
}
